<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use Nette\Object;

/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 02.05.2017
 * Time: 13:34
 */
class SignInFormFactory extends Object
{
    private $user;

    public function injectDependencies(User $user){
        $this->user = $user;
    }

    public function createSignForm(callable $onSuccess){
        $form = new Form(NULL, 'signForm');
        $form->addText('username', 'Username:')
            ->setRequired('Please enter your username.');

        $form->addPassword('password', 'Password:')
            ->setRequired('Please enter your password.');

        $form->addCheckbox('remember', 'Keep me signed in');

        $form->addSubmit('send', 'Sign in');

        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
            try {
                $this->user->setExpiration($values->remember ? '14 days' : '20 minutes');
                $this->user->login($values->username, $values->password);
            } catch (Nette\Security\AuthenticationException $e) {
                $form->addError('The username or password you entered is incorrect.');
                return;
            }
            $onSuccess();
        };

        return $form;

    }
}