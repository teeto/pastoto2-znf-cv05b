<?php
namespace App\Statistic;

use App\Model\StatisticModel;
use Nette\Application\UI\Control;
/**
 * Created by PhpStorm.
 * User: tomas
 * Date: 05.05.2017
 * Time: 17:24
 */
class StatisticControl extends Control {

    /** @var StatisticModel - model pro statistiky */
    private $statisticModel;

    /**
     * Setter pro model statistiky
     * @param StatisticModel $statisticModel automatiky injetovaný model pro správu statistik
     */

    public function __construct( StatisticModel $sm )
    {
        $this->statisticModel = $sm;
    }


    public function render($id)
    {

        $template = $this->template;
        $template->setFile(__DIR__ . '/StatisticControl.latte');
        if($id==NULL){
            $template->statistics = $this->statisticModel->listStatistic();
        }else {
            $template->statistics = $this->statisticModel->listStatisticById($id);
        }
        $template->render();
    }

}