<?php

namespace App\Presenters;

use Nette\Application\IResponse;
use Nette\Security\User;

/**
 * Class HomepagePresenter
 * @package App\Presenters
 */
class HomepagePresenter extends BasePresenter
{
	/** */
	protected function startup()
	{
		parent::startup();
		// TODO: Dokončit cvičení.
	}

	/** */
	public function actionDefault()
	{
	}

	/** */
	protected function beforeRender()
	{
		parent::beforeRender();
		// TODO: Dokončit cvičení.
	}

	/** */
	public function renderDefault()
	{
       /* if (!$this->user->isLoggedIn()) {
            $this->redirect('Sign:in');
        }*/
	}

	/** */
	protected function afterRender()
	{
		parent::afterRender();
		// TODO: Dokončit cvičení.
	}

	/**
	 *
	 * @param IResponse $response
	 */
	protected function shutdown($response)
	{
		parent::shutdown($response);
		// TODO: Dokončit cvičení.
	}
}
