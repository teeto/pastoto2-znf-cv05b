<?php

namespace App\Presenters;

use App\Forms\SignInFormFactory;
use Nette;
use App\Forms;


class SignPresenter extends BasePresenter
{
	/** @var Forms\SignInFormFactory @inject */
	public $signFactory;

    public function injectDependencies(
        SignInFormFactory $formFactory

    )
    {
        $this->signFactory=$formFactory;
    }

	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		return $this->signFactory->createSignForm(function () {
			$this->redirect('Homepage:');
		});
	}


	public function actionOut()
	{
		$this->getUser()->logout();
	}

}
