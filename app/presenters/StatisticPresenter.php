<?php

namespace App\Presenters;

use App\Model\StatisticModel;
use Nette\Application\ForbiddenRequestException;
use Tracy\Debugger;
use App\Statistic;



class StatisticPresenter extends BasePresenter
{

    /** @var StatisticModel - model pro statistiky */
    private $statisticModel;

    /**
     * Setter pro model statistiky
     * @param StatisticModel $statisticModel automatiky injetovaný model pro správu statistik
     */
    public function injectDependencies(
        StatisticModel $statisticModel
    )
    {
        $this->statisticModel = $statisticModel;
    }

    protected function createComponentStatisticControl() {
        $statisticControl = new Statistic\StatisticControl($this->statisticModel);
        return $statisticControl;
    }

    /**
     * Metoda pro naplnění dat pro šablonu dané akce
     */
    public function renderDefault() {
        if (!$this->getUser()->isAllowed('statistic')) {
            throw new ForbiddenRequestException();
        }
       /*$this->template->statistics = $this->statisticModel->listStatistic();*/
    }
}
