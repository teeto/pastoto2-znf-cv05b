<?php
// source: D:\PhpDev\EasyPHP-Devserver-16.1\eds-www\znfcv04b\app\components\statistic/StatisticControl.latte

use Latte\Runtime as LR;

class Template7a63785123 extends Latte\Runtime\Template
{
	public $blocks = [
		'_' => 'block_b14a7',
	];

	public $blockTypes = [
		'_' => 'html',
	];


	function main()
	{
		extract($this->params);
		?><div id="<?php echo htmlSpecialChars($this->global->snippetDriver->getHtmlId('')) ?>"><?php $this->renderBlock('_', $this->params) ?></div><?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['s'])) trigger_error('Variable $s overwritten in foreach on line 24');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function block_b14a7($_args)
	{
		extract($_args);
		$this->global->snippetDriver->enter("", "static");
?>

    <h1>Statistiky</h1>

    <hr>
    <ul class="nav nav-pills">
        <li role="presentation"><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Employer:default")) ?>">Zaměstnanci</a></li>
        <li role="presentation"><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Pid:default")) ?>">Rodná čísla</a></li>
        <li role="presentation"><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Company:default")) ?>">Firmy</a></li>
        <li role="presentation" class="active"><a href="#">Statistiky</a></li>
        <li role="presentation"><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Homepage:default")) ?>">Menu</a></li>
    </ul>

    <div class="row div-table">
        <div class="col-xs-12 div-head">
            <div class="row">
                <div class="col-xs-2">Firma</div>
                <div class="col-xs-3">Minimální plat ve firmě</div>
                <div class="col-xs-2">Maximální plat ve firmě</div>
                <div class="col-xs-2">Průměrný plat ve firmě</div>
                <div class="col-xs-3">Celkový plat ve firmě</div>
            </div>
        </div>
<?php
		$iterations = 0;
		foreach ($statistics as $s) {
?>
            <div class="col-xs-12 div-body">
                <div class="row">
                    <div class="col-xs-2"><?php echo LR\Filters::escapeHtmlText($s['name']) /* line 27 */ ?></div>
                    <div class="col-xs-3"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->number, $s['min'], 2, '.', ' ')) /* line 28 */ ?></div>
                    <div class="col-xs-2"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->number, $s['max'], 2, '.', ' ')) /* line 29 */ ?></div>
                    <div class="col-xs-2"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->number, $s['avg'], 2, '.', ' ')) /* line 30 */ ?></div>
                    <div class="col-xs-3"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->number, $s['sum'], 2, '.', ' ')) /* line 31 */ ?></div>
                </div>
            </div>
<?php
			$iterations++;
		}
?>
    </div>

<?php
		$this->global->snippetDriver->leave();
		
	}

}
