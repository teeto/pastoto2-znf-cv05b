<?php
// source: D:\PhpDev\EasyPHP-Devserver-16.1\eds-www\znfcv04b\app\presenters/templates/Company/add.latte

use Latte\Runtime as LR;

class Template42fbda855d extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<h1>Vložení Firmy</h1>
<p>
<a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("default")) ?>">Zpět</a>
</p>


<?php
		/* line 7 */
		echo Nette\Bridges\FormsLatte\Runtime::renderFormBegin($form = $_form = $this->global->formsStack[] = $this->global->uiControl["addForm"], []);
?>




    <table border="5px dashed">
        <tr class="required">
            <th><?php if ($_label = end($this->global->formsStack)["name"]->getLabel()) echo $_label ?></th>
            <td><?php echo end($this->global->formsStack)["name"]->getControl() /* line 14 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($form['name']->getError()) /* line 15 */ ?></td>
        </tr>

        <tr class="required">
            <th><?php if ($_label = end($this->global->formsStack)["phone"]->getLabel()) echo $_label ?></th>
            <td><?php echo end($this->global->formsStack)["phone"]->getControl() /* line 20 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($form['phone']->getError()) /* line 21 */ ?></td>
        </tr>

        <tr>
            <th><?php if ($_label = end($this->global->formsStack)["is_dph"]->getLabel()) echo $_label ?></th>
            <td><?php echo end($this->global->formsStack)["is_dph"]->getControl() /* line 26 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($form['is_dph']->getError()) /* line 27 */ ?></td>
        </tr>

        <tr>
            <th><?php if ($_label = end($this->global->formsStack)["dane_cislo"]->getLabel()) echo $_label ?></th>
            <td><?php echo end($this->global->formsStack)["dane_cislo"]->getControl() /* line 32 */ ?></td>
            <td><?php echo LR\Filters::escapeHtmlText($form['dane_cislo']->getError()) /* line 33 */ ?></td>
        </tr>

        <tr>
            <th></th>
            <td><?php echo end($this->global->formsStack)["send"]->getControl() /* line 38 */ ?></td>
        </tr>
    </table>
<?php
		echo Nette\Bridges\FormsLatte\Runtime::renderFormEnd(array_pop($this->global->formsStack));
?>

<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}

}
