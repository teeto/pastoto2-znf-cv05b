<?php
// source: D:\PhpDev\EasyPHP-Devserver-16.1\eds-www\znfcv04b\app\presenters/templates/Employer/default.latte

use Latte\Runtime as LR;

class Templateb8221fbd29 extends Latte\Runtime\Template
{
	public $blocks = [
		'content' => 'blockContent',
	];

	public $blockTypes = [
		'content' => 'html',
	];


	function main()
	{
		extract($this->params);
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('content', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['e'])) trigger_error('Variable $e overwritten in foreach on line 31');
		Nette\Bridges\ApplicationLatte\UIRuntime::initialize($this, $this->parentName, $this->blocks);
		
	}


	function blockContent($_args)
	{
		extract($_args);
?>

<h1>Zaměstnanci</h1>
<hr>
<ul class="nav nav-pills">
    <li role="presentation" class="active"><a href="#">Zaměstnanci</a></li>
    <li role="presentation"><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Pid:default")) ?>">Rodná čísla</a></li>
    <li role="presentation"><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Company:default")) ?>">Firmy</a></li>
    <li role="presentation"><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Statistic:default")) ?>">Statistiky</a></li>
    <li role="presentation"><a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("Homepage:default")) ?>">Menu</a></li>
</ul>

<div style="text-align: right">
    <a class="btn btn-success" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("add")) ?>">Vytvoř</a>
</div>

<div class="row div-table">
    <div class="col-xs-12 div-head">
        <div class="row">
            <div class="col-sm-1">Firma</div>
            <div class="col-sm-1">Jméno</div>
            <div class="col-sm-1">Příjmení</div>
            <div class="col-sm-2">Rodné číslo</div>
            <div class="col-sm-1">Pohlaví</div>
            <div class="col-sm-2">Datum narození</div>
            <div class="col-sm-1">Plat</div>
            <div class="col-sm-1">Daň</div>
            <div class="col-sm-2">Akce</div>
        </div>
    </div>
<?php
		$iterations = 0;
		foreach ($employers as $e) {
?>

<?php
			if ($restricted ==-1) {
?>

            <div class="col-xs-12 div-body">
                <div class="row">
                    <div class="col-sm-1"><?php echo LR\Filters::escapeHtmlText($e->company->name) /* line 37 */ ?></div>
                    <div class="col-sm-1"><a href="http://www.kdejsme.cz/jmeno/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($e->firstname)) /* line 38 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($e->firstname) /* line 38 */ ?></a></div>
                    <div class="col-sm-1"><a href="http://www.kdejsme.cz/prijmeni/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($e->surname)) /* line 39 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($e->surname) /* line 39 */ ?></a></div>
                    <div class="col-sm-2">
<?php
				if ($e->pid_id) {
					if (!($utility->isCorrectPid($e->pid->name))) {
						?>                                <strong class="red">!! <?php echo LR\Filters::escapeHtmlText($e->pid->name) /* line 43 */ ?></strong>
<?php
					}
					else {
						?>                                <?php echo LR\Filters::escapeHtmlText($e->pid->name) /* line 45 */ ?>

<?php
					}
				}
				else {
?>
                            Není uvedeno
<?php
				}
?>
                    </div>
                    <div class="col-sm-1">
<?php
				if ($e->pid_id) {
					$isMan = $utility->isMan($e->pid_id);
					if (($isMan !== -1)) {
						?>                                <?php echo LR\Filters::escapeHtmlText($isMan ? 'Muž' : 'Žena') /* line 55 */ ?>

<?php
					}
					else {
?>
                                <strong class="red">!!</strong>
<?php
					}
				}
				else {
?>
                            Není uvedeno
<?php
				}
?>
                    </div>
                    <div class="col-sm-2">
<?php
				if ($e->pid_id) {
					$getBirthDay = $utility->getBirthDay($e->pid_id);
					if (($getBirthDay) != -1) {
						?>                                <?php echo LR\Filters::escapeHtmlText($getBirthDay) /* line 67 */ ?>

<?php
					}
					else {
?>
                                <strong class="red">!!</strong>
<?php
					}
				}
				else {
?>
                            Není uvedeno
<?php
				}
?>
                    </div>
                    <div class="col-sm-1"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->number, $e->salary, 2, '.', ' ')) /* line 75 */ ?></div>
                    <div class="col-sm-1"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->number, $e->salary * 0.22, 2, '.', ' ')) /* line 76 */ ?></div>
                    <div class="col-sm-2">
                        <a class="btn btn-warning" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("edit", ['id' => $e->id])) ?>">Edituj</a>
                        <a class="btn btn-danger" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("delete", ['id' => $e->id])) ?>">Odeber</a>
                        <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("detail", ['id' => $e->id])) ?>">Statistika</a>
                    </div>
                </div>
            </div>
<?php
			}
			elseif ($restricted == $e->id) {
?>
                <div class="col-xs-12 div-body">
                    <div class="row">
                        <div class="col-sm-1"><?php echo LR\Filters::escapeHtmlText($e->company->name) /* line 87 */ ?></div>
                        <div class="col-sm-1"><a href="http://www.kdejsme.cz/jmeno/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($e->firstname)) /* line 88 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($e->firstname) /* line 88 */ ?></a></div>
                        <div class="col-sm-1"><a href="http://www.kdejsme.cz/prijmeni/<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($e->surname)) /* line 89 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($e->surname) /* line 89 */ ?></a></div>
                        <div class="col-sm-2">
<?php
				if ($e->pid_id) {
					if (!($utility->isCorrectPid($e->pid->name))) {
						?>                                    <strong class="red">!! <?php echo LR\Filters::escapeHtmlText($e->pid->name) /* line 93 */ ?></strong>
<?php
					}
					else {
						?>                                    <?php echo LR\Filters::escapeHtmlText($e->pid->name) /* line 95 */ ?>

<?php
					}
				}
				else {
?>
                                Není uvedeno
<?php
				}
?>
                        </div>
                        <div class="col-sm-1">
<?php
				if ($e->pid_id) {
					$isMan = $utility->isMan($e->pid_id);
					if (($isMan !== -1)) {
						?>                                    <?php echo LR\Filters::escapeHtmlText($isMan ? 'Muž' : 'Žena') /* line 105 */ ?>

<?php
					}
					else {
?>
                                    <strong class="red">!!</strong>
<?php
					}
				}
				else {
?>
                                Není uvedeno
<?php
				}
?>
                        </div>
                        <div class="col-sm-2">
<?php
				if ($e->pid_id) {
					$getBirthDay = $utility->getBirthDay($e->pid_id);
					if (($getBirthDay) != -1) {
						?>                                    <?php echo LR\Filters::escapeHtmlText($getBirthDay) /* line 117 */ ?>

<?php
					}
					else {
?>
                                    <strong class="red">!!</strong>
<?php
					}
				}
				else {
?>
                                Není uvedeno
<?php
				}
?>
                        </div>
                        <div class="col-sm-1"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->number, $e->salary, 2, '.', ' ')) /* line 125 */ ?></div>
                        <div class="col-sm-1"><?php echo LR\Filters::escapeHtmlText(call_user_func($this->filters->number, $e->salary * 0.22, 2, '.', ' ')) /* line 126 */ ?></div>
                        <div class="col-sm-2">
                            <a class="btn btn-warning" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("edit", ['id' => $e->id])) ?>">Edituj</a>
                            <a class="btn btn-danger" href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("delete", ['id' => $e->id])) ?>">Odeber</a>
                            <a href="<?php echo LR\Filters::escapeHtmlAttr($this->global->uiPresenter->link("detail", ['id' => $e->id])) ?>">Statistika</a>
                        </div>
                    </div>
                </div>
<?php
			}
			$iterations++;
		}
		?></div><?php
	}

}
